# sisop-praktikum-modul-4-2023-AM-B10

Anggota :
| Nama                         | NRP        |
|------------------------------|------------|
|Raden Pandu Anggono Rasyid    | 5025201024 |
|Ryan Abinugraha               | 5025211178 |
|Abhinaya Radiansyah Listiyanto| 5025211173 |

### Penjelasan No 1
Pertama download dataset
```
pip install kaggle

```
memberikan izin akses yang tepat untuk file kaggle.json.
```
chmod 600 ~/.kaggle/kaggle.json

```
download dataset

```
kaggle datasets download -d bryanb/fifa-player-stats-database

```
setelah dataset didapatkan, extract file melalui program C yang bernama storage.c dengan bantuan library zlib yang ditempatkan pada direktori bernama modul4
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <zlib.h>

#define CHUNK_SIZE 1024

int extractZipFile(const char* zipFilePath, const char* destinationDir) {
    int ret;
    unsigned char buffer[CHUNK_SIZE];
    int bufferSize = CHUNK_SIZE;
    
    // Buka file zip
    FILE* zipFile = fopen(zipFilePath, "rb");
    if (!zipFile) {
        printf("Failed to open zip file.\n");
        return -1;
    }

    // Inisialisasi objek zip
    z_stream zipStream;
    memset(&zipStream, 0, sizeof(z_stream));
    ret = inflateInit(&zipStream);
    if (ret != Z_OK) {
        printf("Failed to initialize zip stream.\n");
        fclose(zipFile);
        return -1;
    }
    
    // Baca file zip dan ekstrak
    ret = inflateInit2(&zipStream, -MAX_WBITS);
    if (ret != Z_OK) {
        printf("Failed to initialize zip stream.\n");
        inflateEnd(&zipStream);
        fclose(zipFile);
        return -1;
    }

    char outputPath[256];
    sprintf(outputPath, "%s/storage.c", destinationDir);

    FILE* outputFile = fopen(outputPath, "wb");
    if (!outputFile) {
        printf("Failed to create output file.\n");
        inflateEnd(&zipStream);
        fclose(zipFile);
        return -1;
    }

    do {
        zipStream.avail_in = fread(buffer, 1, bufferSize, zipFile);
        if (zipStream.avail_in == 0) {
            break;
        }
        zipStream.next_in = buffer;

        do {
            zipStream.avail_out = bufferSize;
            zipStream.next_out = buffer;
            ret = inflate(&zipStream, Z_NO_FLUSH);
            if (ret != Z_OK && ret != Z_STREAM_END) {
                printf("Failed to extract zip file.\n");
                inflateEnd(&zipStream);
                fclose(zipFile);
                fclose(outputFile);
                return -1;
            }
            int writeSize = bufferSize - zipStream.avail_out;
            fwrite(buffer, 1, writeSize, outputFile);
        } while (zipStream.avail_out == 0);
    } while (ret != Z_STREAM_END);

    // Tutup file dan objek zip
    inflateEnd(&zipStream);
    fclose(zipFile);
    fclose(outputFile);

    printf("File extracted successfully.\n");
    return 0;
}

int main() {
    const char* zipFilePath = "/home/pandu/fifa-player-stats-database.zip";
    const char* destinationDir = "/modul4/fifa-player-stats-database";
    
    int ret = extractZipFile(zipFilePath, destinationDir);
    if (ret != 0) {
        printf("Extraction failed.\n");
        return 1;
    }

    return 0;
}

```
membaca file CSV khusus bernama FIFA23_official_data.csv

dan mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain Manchester City. Informasi yang dicetak mencakup nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya.

```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LINE_LENGTH 1000
#define CSV_DELIMITER ","

typedef struct {
    char name[100];
    char club[100];
    int age;
    int potential;
    char photoUrl[200];
    // tambahkan data lain yang diperlukan
} Player;

void printPlayerData(Player player) {
    printf("Name: %s\n", player.name);
    printf("Club: %s\n", player.club);
    printf("Age: %d\n", player.age);
    printf("Potential: %d\n", player.potential);
    printf("Photo URL: %s\n", player.photoUrl);
    // tambahkan data lain yang ingin dicetak
    printf("\n");
}

int main() {
    FILE *file;
    char line[MAX_LINE_LENGTH];
    char *token;
    Player player;

    // Buka file CSV
    file = fopen("FIFA23_official_data.csv", "r");
    if (file == NULL) {
        printf("Failed to open CSV file.\n");
        return 1;
    }

    // Baca setiap baris dalam file CSV
    while (fgets(line, MAX_LINE_LENGTH, file)) {
        // Pisahkan data dalam baris menggunakan delimiter ","
        token = strtok(line, CSV_DELIMITER);

        // Baca dan simpan data pemain
        strcpy(player.name, token);
        token = strtok(NULL, CSV_DELIMITER);
        strcpy(player.club, token);
        token = strtok(NULL, CSV_DELIMITER);
        player.age = atoi(token);
        token = strtok(NULL, CSV_DELIMITER);
        player.potential = atoi(token);
        token = strtok(NULL, CSV_DELIMITER);
        strcpy(player.photoUrl, token);

        // Cek kriteria pemain
        if (player.age < 25 && player.potential > 85 && strcmp(player.club, "Manchester City") != 0) {
            // Cetak data pemain yang memenuhi kriteria
            printPlayerData(player);
        }
    }

    // Tutup file
    fclose(file);

    return 0;
}

```
seetelah itu compile dan jalankan

```
gcc storage.c -o storage
./storage

```
jadikan sistem yang dibuat ke sebuah Docker Container agar mudah di-distribute dan dijalankan di lingkungan lain tanpa perlu setup environment dari awal.

```
# Menggunakan image Ubuntu sebagai base
FROM ubuntu

# Update repository dan menginstal Python 3
RUN apt update && apt install -y python3 python3-pip

RUN apt-get update && apt-get install -y unzip

# Menjalankan Python 3 saat container berjalan
CMD ["python3"]

# Menginstal Kaggle CLI
RUN pip install kaggle

# Mengunduh file konfigurasi Kaggle
RUN mkdir /root/.kaggle && \
    echo '{"username":"pandu","key":"Rasyid28"}' > /root/.kaggle/kaggle.json && \
    chmod 600 /root/.kaggle/kaggle.json

# Mengkompilasi kode sumber
COPY storage.c /app/
WORKDIR /app
RUN gcc -o storage storage.c

# Menjalankan aplikasi saat container berjalan
CMD ["./storage"]

```
### Penjelasan No 2

code for germa.c 
```
// modul chaotic
// gcc -Wall -fstack-protector germa.c `pkg-config fuse --cflags --libs` -o germa
// ./germa -f nanaxgerma/dest_data

#define FUSE_USE_VERSION 31
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <time.h>
#include <pwd.h>
#include <sys/types.h>

static const char *dirpath = "/home/ryanabi/Documents/PrakSIsop4/logmucatatsini.txt";
char logFilePath[1000] = ""; // File path for log file
FILE *logFile = NULL;        // Pointer to the log file

void writeLog(const char *status, const char *cmd, const char *desc)
{
  struct passwd *pws;
  int uid = getuid();
  pws = getpwuid(uid);

  time_t now;
  struct tm *timestamp;
  char datetime[200];

  // Get current timestamp
  time(&now);
  timestamp = localtime(&now);

  // Format the timestamp
  strftime(datetime, sizeof(datetime), "%d/%m/%Y-%H:%M:%S", timestamp);

  // Create log entry string
  char log_entry[2000];
  sprintf(log_entry, "%s::%s::%s::%s-%s", status, datetime, cmd, pws->pw_name, desc);

  snprintf(logFilePath, sizeof(logFilePath), "%s", "logmucatatsini.txt");
  logFile = fopen(logFilePath, "a");
  if (logFile == NULL)
  {
    perror("Failed to open log file");
  }

  fprintf(logFile, "%s\n", log_entry);

  fclose(logFile);

  // SUCCESS::17/05/2023-19:31:56::RENAME::Oky-Rename from x to y
}

int isRestricted(const char *path)
{
  return strstr(path, "restricted") != NULL;
}

int isBypass(const char *path)
{
  return strstr(path, "bypass") != NULL;
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
  int res;
  char fpath[1000];

  sprintf(fpath, "%s%s", dirpath, path);

  res = lstat(fpath, stbuf);

  if (res == -1)
    return -errno;

  return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
  char fpath[1000];

  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  int res = 0;

  DIR *dp;
  struct dirent *de;
  (void)offset;
  (void)fi;

  dp = opendir(fpath);

  if (dp == NULL)
    return -errno;

  while ((de = readdir(dp)) != NULL)
  {
    struct stat st;

    memset(&st, 0, sizeof(st));

    st.st_ino = de->d_ino;
    st.st_mode = de->d_type << 12;
    res = (filler(buf, de->d_name, &st, 0));

    // char desc[2000];
    // snprintf(desc, sizeof(desc), "Read directory %s", fpath);
    // writeLog("SUCCESS", "READDIR", desc);

    if (res != 0)
    {
      // writeLog("FAILED", "READDIR", desc);
      break;
    }
  }

  closedir(dp);

  return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
  char fpath[1000];
  if (strcmp(path, "/") == 0)
  {
    path = dirpath;

    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  int res = 0;
  int fd = 0;

  (void)fi;

  fd = open(fpath, O_RDONLY);

  if (fd == -1)
    return -errno;

  res = pread(fd, buf, size, offset);

  // char desc[2000];
  // snprintf(desc, sizeof(desc), "Read file %s", fpath);
  // writeLog("SUCCESS", "READ", desc);

  if (res == -1)
  {
    // writeLog("FAILED", "READ", desc);
    res = -errno;
  }

  close(fd);

  return res;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
  printf("doing mkdir\n");
  char fpath[1000];
  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  int res = 0;

  if (!isRestricted(fpath) || isBypass(fpath))
  {
    res = mkdir(fpath, mode);

    char desc[2000];
    snprintf(desc, sizeof(desc), "Create directory %s", fpath);
    writeLog("SUCCESS", "MKDIR", desc);
  }
  else
  {
    res = -EPERM;

    char desc[2000];
    snprintf(desc, sizeof(desc), "Create directory %s", fpath);
    writeLog("FAILED", "MKDIR", desc);
  }

  return res;
}

static int xmp_rename(const char *from, const char *to)
{
  printf("doing rename\n");

  char oldPath[1000], newPath[1000];
  snprintf(oldPath, sizeof(oldPath), "%s%s", dirpath, from);
  snprintf(newPath, sizeof(newPath), "%s%s", dirpath, to);

  int res = 0;
  if (!isRestricted(oldPath) || !isRestricted(newPath))
  {
    res = rename(oldPath, newPath);

    char desc[2500];
    snprintf(desc, sizeof(desc), "Rename from %s to %s", oldPath, newPath);
    writeLog("SUCCESS", "RENAME", desc);
  }
  else
  {
    res = -errno;

    char desc[2500];
    snprintf(desc, sizeof(desc), "Rename from %s to %s", oldPath, newPath);
    writeLog("FAILED", "RENAME", desc);
  }

  return res;
}

static int xmp_rmdir(const char *path)
{
  printf("doing rmdir\n");

  char fpath[1000];
  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  int res = 0;

  if (!isRestricted(fpath) || isBypass(fpath))
  {
    res = rmdir(fpath);

    char desc[2000];
    snprintf(desc, sizeof(desc), "Remove directory %s", fpath);
    writeLog("SUCCESS", "RMDIR", desc);
  }
  else
  {
    char desc[2000];
    snprintf(desc, sizeof(desc), "Remove directory %s", fpath);
    writeLog("FAILED", "RMDIR", desc);
    res = -errno;
  }

  return res;
}

static int xmp_unlink(const char *path)
{
  printf("doing unlink\n");

  char fpath[1000];
  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  int res = 0;

  if (!isRestricted(fpath) || isBypass(fpath))
  {
    res = unlink(fpath);

    char desc[2000];
    snprintf(desc, sizeof(desc), "Remove file %s", fpath);
    writeLog("SUCCESS", "UNLINK", desc);
  }
  else
  {
    res = -EPERM;

    char desc[2000];
    snprintf(desc, sizeof(desc), "Remove file %s", fpath);
    writeLog("FAILED", "UNLINK", desc);
  }

  return res;
}

static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
  printf("doing create\n");

  char fpath[1000];
  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  int res = 0;

  if (!isRestricted(fpath) || isBypass(fpath))
  {
    res = creat(fpath, mode);

    char desc[2000];
    snprintf(desc, sizeof(desc), "Create file %s", fpath);
    writeLog("SUCCESS", "CREATE", desc);
  }
  else
  {
    res = -EPERM;

    char desc[2000];
    snprintf(desc, sizeof(desc), "Create file %s", fpath);
    writeLog("FAILED", "CREATE", desc);
  }

  fi->fh = res;

  return res;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
    .rmdir = xmp_rmdir,
    .unlink = xmp_unlink,
    .create = xmp_create
};

int main(int argc, char *argv[])
{
  umask(0);

  return fuse_main(argc, argv, &xmp_oper, NULL);
}
```

### Penjelasan No 3
Dalam soal diminta kita untuk mount folder yang berisi beberapa folder dan mengencode menggunakan Base64
```
if (!base64Output) {
      fprintf(stderr, "Gagal Mengalokasikan memori.\n");
      fclose(file);
      free(fileContent);
      return;
   }

   // Encode file ke base64
   int i, j;
   for (i = 0, j = 0; i < fileSize; i += 3, j += 4) {
      unsigned char byte1 = fileContent[i];
      unsigned char byte2 = (i + 1 < fileSize) ? fileContent[i + 1] : 0;
      unsigned char byte3 = (i + 2 < fileSize) ? fileContent[i + 2] : 0;

      unsigned char index1 = byte1 >> 2;
      unsigned char index2 = ((byte1 & 0x03) << 4) | (byte2 >> 4);
      unsigned char index3 = ((byte2 & 0x0F) << 2) | (byte3 >> 6);
      unsigned char index4 = byte3 & 0x3F;

      base64Output[j] = base64Chars[index1];
      base64Output[j + 1] = base64Chars[index2];
      base64Output[j + 2] = (i + 1 < fileSize) ? base64Chars[index3] : '=';
      base64Output[j + 3] = (i + 2 < fileSize) ? base64Chars[index4] : '=';
   }
   base64Output[j] = '\0';

   // tutup file
   fclose(file)
```
dalam kode diatas menjelaskan fungsi fungsi untuk mengencode file filw yang ingin di Encode
```
static int encode_getattr(const char *path, struct stat *stbuf) {
       int res;
           char full_path[PATH_MAX];

               sprintf(full_path, "%s%s", fuse_get_context()->private_data, path);

res = lstat(full_path, stbuf);
if (res == -1)
return -errno;

    return 0;
    
}
```
Code diatas menjelaskan fungsi untuk mendapatkan data data
```
static int encode_open(const char *path, struct fuse_file_info *fi) {
if (strcmp(password, "") != 0) {
    printf("Enter password: ");
    char entered_password[50];
    scanf("%s", entered_password);
 if (strcmp(entered_password, password) != 0)                           return -EACCES;  // Wrong password, deny access                            }

    int res;
     char full_path[PATH_MAX]                           sprintf(full_path, "%s%s", fuse_get_context()->private_data, path);
 res = open(full_path, fi->flags);
if (res == -1)
    return -errno;
    close(res);
    return 0;
    }
}
```
Code diatas menjelaskan bagaimana untuk jika mount file harus memasukkan password terlebih dahulu

Bagian Revisi :
bagian revisi pada code tersebut adalah bagian fungsi yang diminta encode ASCII
```
void formatAscii(char *name) {
      if (strlen(name) <= 4) {
            char *newName = (char*) malloc(8 * strlen(name) * sizeof(char)); // Alokasikan memori yang cukup untuk string baru
                  int div;
                        size_t index = 0;
                              for (int i = 0; i < strlen(name); i++) {
                                       div = 128; // 8th bit
                                                while (div) {
                                                            newName[index] = ((name[i] & div) == div) ? '1' : '0';
                                                                        index++;
                                                                                    if (div == 1 && i < strlen(name) - 1) {
                                                                                                   newName[index] = ' ';
                                                                                                                  index++;
                                                                                                                              }
                                                                                                                                          div /= 2;
                                                                                                                                                   }
                                                                                                                                                         }
    newName[index] = '\0';
      strcpy(name, newName);
    free(newName); // Bebaskan memori yang dialokasikan untuk string baru
        }
    }
}
```
