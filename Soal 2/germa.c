// modul chaotic
// gcc -Wall -fstack-protector germa.c `pkg-config fuse --cflags --libs` -o germa
// ./germa -f nanaxgerma/dest_data

#define FUSE_USE_VERSION 31
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <time.h>
#include <pwd.h>
#include <sys/types.h>

static const char *dirpath = "/home/ryanabi/Documents/PrakSIsop4/logmucatatsini.txt";
char logFilePath[1000] = ""; // File path for log file
FILE *logFile = NULL;        // Pointer to the log file

void writeLog(const char *status, const char *cmd, const char *desc)
{
  struct passwd *pws;
  int uid = getuid();
  pws = getpwuid(uid);

  time_t now;
  struct tm *timestamp;
  char datetime[200];

  // Get current timestamp
  time(&now);
  timestamp = localtime(&now);

  // Format the timestamp
  strftime(datetime, sizeof(datetime), "%d/%m/%Y-%H:%M:%S", timestamp);

  // Create log entry string
  char log_entry[2000];
  sprintf(log_entry, "%s::%s::%s::%s-%s", status, datetime, cmd, pws->pw_name, desc);

  snprintf(logFilePath, sizeof(logFilePath), "%s", "logmucatatsini.txt");
  logFile = fopen(logFilePath, "a");
  if (logFile == NULL)
  {
    perror("Failed to open log file");
  }

  fprintf(logFile, "%s\n", log_entry);

  fclose(logFile);

  // SUCCESS::17/05/2023-19:31:56::RENAME::Oky-Rename from x to y
}

int isRestricted(const char *path)
{
  return strstr(path, "restricted") != NULL;
}

int isBypass(const char *path)
{
  return strstr(path, "bypass") != NULL;
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
  int res;
  char fpath[1000];

  sprintf(fpath, "%s%s", dirpath, path);

  res = lstat(fpath, stbuf);

  if (res == -1)
    return -errno;

  return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
  char fpath[1000];

  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  int res = 0;

  DIR *dp;
  struct dirent *de;
  (void)offset;
  (void)fi;

  dp = opendir(fpath);

  if (dp == NULL)
    return -errno;

  while ((de = readdir(dp)) != NULL)
  {
    struct stat st;

    memset(&st, 0, sizeof(st));

    st.st_ino = de->d_ino;
    st.st_mode = de->d_type << 12;
    res = (filler(buf, de->d_name, &st, 0));

    // char desc[2000];
    // snprintf(desc, sizeof(desc), "Read directory %s", fpath);
    // writeLog("SUCCESS", "READDIR", desc);

    if (res != 0)
    {
      // writeLog("FAILED", "READDIR", desc);
      break;
    }
  }

  closedir(dp);

  return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
  char fpath[1000];
  if (strcmp(path, "/") == 0)
  {
    path = dirpath;

    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  int res = 0;
  int fd = 0;

  (void)fi;

  fd = open(fpath, O_RDONLY);

  if (fd == -1)
    return -errno;

  res = pread(fd, buf, size, offset);

  // char desc[2000];
  // snprintf(desc, sizeof(desc), "Read file %s", fpath);
  // writeLog("SUCCESS", "READ", desc);

  if (res == -1)
  {
    // writeLog("FAILED", "READ", desc);
    res = -errno;
  }

  close(fd);

  return res;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
  printf("doing mkdir\n");
  char fpath[1000];
  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  int res = 0;

  if (!isRestricted(fpath) || isBypass(fpath))
  {
    res = mkdir(fpath, mode);

    char desc[2000];
    snprintf(desc, sizeof(desc), "Create directory %s", fpath);
    writeLog("SUCCESS", "MKDIR", desc);
  }
  else
  {
    res = -EPERM;

    char desc[2000];
    snprintf(desc, sizeof(desc), "Create directory %s", fpath);
    writeLog("FAILED", "MKDIR", desc);
  }

  return res;
}

static int xmp_rename(const char *from, const char *to)
{
  printf("doing rename\n");

  char oldPath[1000], newPath[1000];
  snprintf(oldPath, sizeof(oldPath), "%s%s", dirpath, from);
  snprintf(newPath, sizeof(newPath), "%s%s", dirpath, to);

  int res = 0;
  if (!isRestricted(oldPath) || !isRestricted(newPath))
  {
    res = rename(oldPath, newPath);

    char desc[2500];
    snprintf(desc, sizeof(desc), "Rename from %s to %s", oldPath, newPath);
    writeLog("SUCCESS", "RENAME", desc);
  }
  else
  {
    res = -errno;

    char desc[2500];
    snprintf(desc, sizeof(desc), "Rename from %s to %s", oldPath, newPath);
    writeLog("FAILED", "RENAME", desc);
  }

  return res;
}

static int xmp_rmdir(const char *path)
{
  printf("doing rmdir\n");

  char fpath[1000];
  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  int res = 0;

  if (!isRestricted(fpath) || isBypass(fpath))
  {
    res = rmdir(fpath);

    char desc[2000];
    snprintf(desc, sizeof(desc), "Remove directory %s", fpath);
    writeLog("SUCCESS", "RMDIR", desc);
  }
  else
  {
    char desc[2000];
    snprintf(desc, sizeof(desc), "Remove directory %s", fpath);
    writeLog("FAILED", "RMDIR", desc);
    res = -errno;
  }

  return res;
}

static int xmp_unlink(const char *path)
{
  printf("doing unlink\n");

  char fpath[1000];
  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  int res = 0;

  if (!isRestricted(fpath) || isBypass(fpath))
  {
    res = unlink(fpath);

    char desc[2000];
    snprintf(desc, sizeof(desc), "Remove file %s", fpath);
    writeLog("SUCCESS", "UNLINK", desc);
  }
  else
  {
    res = -EPERM;

    char desc[2000];
    snprintf(desc, sizeof(desc), "Remove file %s", fpath);
    writeLog("FAILED", "UNLINK", desc);
  }

  return res;
}

static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
  printf("doing create\n");

  char fpath[1000];
  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  int res = 0;

  if (!isRestricted(fpath) || isBypass(fpath))
  {
    res = creat(fpath, mode);

    char desc[2000];
    snprintf(desc, sizeof(desc), "Create file %s", fpath);
    writeLog("SUCCESS", "CREATE", desc);
  }
  else
  {
    res = -EPERM;

    char desc[2000];
    snprintf(desc, sizeof(desc), "Create file %s", fpath);
    writeLog("FAILED", "CREATE", desc);
  }

  fi->fh = res;

  return res;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
    .rmdir = xmp_rmdir,
    .unlink = xmp_unlink,
    .create = xmp_create
};

int main(int argc, char *argv[])
{
  umask(0);

  return fuse_main(argc, argv, &xmp_oper, NULL);
}