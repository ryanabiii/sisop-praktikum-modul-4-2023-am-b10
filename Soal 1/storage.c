//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//#include <zlib.h>
//
//#define CHUNK_SIZE 1024
//
//int extractZipFile(const char* zipFilePath, const char* destinationDir) {
//    int ret;
//    unsigned char buffer[CHUNK_SIZE];
//    int bufferSize = CHUNK_SIZE;
//    
//    // Buka file zip
//    FILE* zipFile = fopen(zipFilePath, "rb");
//    if (!zipFile) {
//        printf("Failed to open zip file.\n");
//        return -1;
//    }
//
//    // Inisialisasi objek zip
//    z_stream zipStream;
//    memset(&zipStream, 0, sizeof(z_stream));
//    ret = inflateInit(&zipStream);
//    if (ret != Z_OK) {
//        printf("Failed to initialize zip stream.\n");
//        fclose(zipFile);
//        return -1;
//    }
//    
//    // Baca file zip dan ekstrak
//    ret = inflateInit2(&zipStream, -MAX_WBITS);
//    if (ret != Z_OK) {
//        printf("Failed to initialize zip stream.\n");
//        inflateEnd(&zipStream);
//        fclose(zipFile);
//        return -1;
//    }
//
//    char outputPath[256];
//    sprintf(outputPath, "%s/storage.c", destinationDir);
//
//    FILE* outputFile = fopen(outputPath, "wb");
//    if (!outputFile) {
//        printf("Failed to create output file.\n");
//        inflateEnd(&zipStream);
//        fclose(zipFile);
//        return -1;
//    }
//
//    do {
//        zipStream.avail_in = fread(buffer, 1, bufferSize, zipFile);
//        if (zipStream.avail_in == 0) {
//            break;
//        }
//        zipStream.next_in = buffer;
//
//        do {
//            zipStream.avail_out = bufferSize;
//            zipStream.next_out = buffer;
//            ret = inflate(&zipStream, Z_NO_FLUSH);
//            if (ret != Z_OK && ret != Z_STREAM_END) {
//                printf("Failed to extract zip file.\n");
//                inflateEnd(&zipStream);
//                fclose(zipFile);
//                fclose(outputFile);
//                return -1;
//            }
//            int writeSize = bufferSize - zipStream.avail_out;
//            fwrite(buffer, 1, writeSize, outputFile);
//        } while (zipStream.avail_out == 0);
//    } while (ret != Z_STREAM_END);
//
//    // Tutup file dan objek zip
//    inflateEnd(&zipStream);
//    fclose(zipFile);
//    fclose(outputFile);
//
//    printf("File extracted successfully.\n");
//    return 0;
//}
//
//int main() {
//    const char* zipFilePath = "/home/pandu/fifa-player-stats-database.zip";
//    const char* destinationDir = "/home/pandu/modul4";
//    
//    int ret = extractZipFile(zipFilePath, destinationDir);
//    if (ret != 0) {
//        printf("Extraction failed.\n");
//        return 1;
//    }
//
//    return 0;
//}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LINE_LENGTH 1000
#define CSV_DELIMITER ","

typedef struct {
    char name[100];
    char club[100];
    int age;
    int potential;
    char photoUrl[200];
    
} Player;

void printPlayerData(Player player) {
    printf("Name: %s\n", player.name);
    printf("Club: %s\n", player.club);
    printf("Age: %d\n", player.age);
    printf("Potential: %d\n", player.potential);
    printf("Photo URL: %s\n", player.photoUrl);
    
    printf("\n");
}

int main() {
    FILE *file;
    char line[MAX_LINE_LENGTH];
    char *token;
    Player player;

    // Buka file CSV
    file = fopen("FIFA23_official_data.csv", "r");
    if (file == NULL) {
        printf("Failed to open CSV file.\n");
        return 1;
    }

    // Baca setiap baris dalam file CSV
    while (fgets(line, MAX_LINE_LENGTH, file)) {
        // Pisahkan data dalam baris menggunakan delimiter ","
        token = strtok(line, CSV_DELIMITER);

        // Baca dan simpan data pemain
        strcpy(player.name, token);
        token = strtok(NULL, CSV_DELIMITER);
        strcpy(player.club, token);
        token = strtok(NULL, CSV_DELIMITER);
        player.age = atoi(token);
        token = strtok(NULL, CSV_DELIMITER);
        player.potential = atoi(token);
        token = strtok(NULL, CSV_DELIMITER);
        strcpy(player.photoUrl, token);

        // Cek kriteria pemain
        if (player.age < 25 && player.potential > 85 && strcmp(player.club, "Manchester City") != 0) {
            // Cetak data pemain yang memenuhi kriteria
            printPlayerData(player);
        }
    }

    // Tutup file
    fclose(file);

    return 0;
}

