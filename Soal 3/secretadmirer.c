#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <ctype.h>

static
const char * dirpath = "/home/vagrant/Documents/sisop";
static
const char * password = "password1234";

const char base64Chars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

// Fungsi untuk encode file
void encodeFile(const char * filePath) {
   FILE * file = fopen(filePath, "rb");
   if (!file) {
      fprintf(stderr, "Gagal membuka file: %s\n", filePath);
      return;
   }

   // hitung ukuran file
   fseek(file, 0, SEEK_END);
   long fileSize = ftell(file);
   fseek(file, 0, SEEK_SET);

   // Alokasikan memory untuk isi file
   unsigned char * fileContent = malloc(fileSize);
   if (!fileContent) {
      fprintf(stderr, "Gagal mengalokasikan memori.\n");
      fclose(file);
      return;
   }

   // baca isi file
   if (fread(fileContent, 1, fileSize, file) != fileSize) {
      fprintf(stderr, "Gagal membaca file: %s\n", filePath);
      fclose(file);
      free(fileContent);
      return;
   }

   // hitung ukuran output base64
   long outputSize = 4 * ((fileSize + 2) / 3);

   // alokasikan memori untuk output base64
   char * base64Output = malloc(outputSize + 1);
   if (!base64Output) {
      fprintf(stderr, "Gagal Mengalokasikan memori.\n");
      fclose(file);
      free(fileContent);
      return;
   }

   // Encode file ke base64
   int i, j;
   for (i = 0, j = 0; i < fileSize; i += 3, j += 4) {
      unsigned char byte1 = fileContent[i];
      unsigned char byte2 = (i + 1 < fileSize) ? fileContent[i + 1] : 0;
      unsigned char byte3 = (i + 2 < fileSize) ? fileContent[i + 2] : 0;

      unsigned char index1 = byte1 >> 2;
      unsigned char index2 = ((byte1 & 0x03) << 4) | (byte2 >> 4);
      unsigned char index3 = ((byte2 & 0x0F) << 2) | (byte3 >> 6);
      unsigned char index4 = byte3 & 0x3F;

      base64Output[j] = base64Chars[index1];
      base64Output[j + 1] = base64Chars[index2];
      base64Output[j + 2] = (i + 1 < fileSize) ? base64Chars[index3] : '=';
      base64Output[j + 3] = (i + 2 < fileSize) ? base64Chars[index4] : '=';
   }
   base64Output[j] = '\0';

   // tutup file
   fclose(file);
}

void formatAscii(char *name) {
   if (strlen(name) <= 4) {
      char *newName = (char*) malloc(8 * strlen(name) * sizeof(char)); // Alokasikan memori yang cukup untuk string baru
      int div;
      size_t index = 0;
      for (int i = 0; i < strlen(name); i++) {
         div = 128; // 8th bit
         while (div) {
            newName[index] = ((name[i] & div) == div) ? '1' : '0';
            index++;
            if (div == 1 && i < strlen(name) - 1) {
               newName[index] = ' ';
               index++;
            }
            div /= 2;
         }
      }
      newName[index] = '\0';
      strcpy(name, newName);
      free(newName); // Bebaskan memori yang dialokasikan untuk string baru
   }
}


static int encode_getattr(const char *path, struct stat *stbuf) {
    int res;
    char full_path[PATH_MAX];

    sprintf(full_path, "%s%s", fuse_get_context()->private_data, path);

    res = lstat(full_path, stbuf);
    if (res == -1)
        return -errno;

    return 0;
}

static int encode_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(path);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        if(filler(buf, de->d_name, &st, 0)) break;
    }
    closedir(dp);
    return 0;
}

static int encode_open(const char *path, struct fuse_file_info *fi) {
    if (strcmp(password, "") != 0) {
        printf("Enter password: ");
        char entered_password[50];
        scanf("%s", entered_password);
        if (strcmp(entered_password, password) != 0)
            return -EACCES;  // Wrong password, deny access
    }

    int res;
    char full_path[PATH_MAX];

    sprintf(full_path, "%s%s", fuse_get_context()->private_data, path);

    res = open(full_path, fi->flags);
    if (res == -1)
        return -errno;

    close(res);
    return 0;
}

static struct fuse_operations encode_oper = {
    .getattr = encode_getattr,
    .readdir = encode_readdir,
    .open = encode_open,
};

int main(int argc, char *argv[]) {
    umask(0);
    return fuse_main(argc, argv, &encode_oper, NULL);
}
